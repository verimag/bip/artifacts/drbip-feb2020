## DR-BIP Platoon System Artifact

Active repository for running and illustrating DR-BIP on the Platoon System.


The repository is self-contained and requires Java and CMAKE to be installed with optional dependencies on Python3 (for visualizing traces) and R (for benchmark analysis and plotting).

Check the sub-folder README.md files for further instructions.
All README.md files are pre-rendered as HTML for convenience.
In each directory (including this one), the readme.html file corresponds to a rendered README.md.

1. [platoon](platoon) contains the main platoon case study model and executions to execute and simulate it.
1. [plot](plot) contains data for the performance analysis as well as the R scripts used to process and plot it.
1. [tools](tools) contains the tools needed to run DR-BIP and view traces.


>  The artifact is intended to be run on Linux systems. However the tools should work on most systems, check Makefile and adapt scripts accordingly.
