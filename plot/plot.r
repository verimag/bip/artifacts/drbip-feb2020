# Common Plotting
source("common.r")

# Set up graphs
cols<- "OrRd"
leg <- "none"

# Read data and process it by doing basic replacements and values ordering
dat<-read.csv("bench.csv", header = TRUE)




# Generate Data Frame for metrics graphed

res<- dat %>%
    group_by(freq,cars, dist, dynamic) %>%
  summarise(
    timebip=mean(timebip),
    timedrbip=mean(timedrbip),
    timetotal=mean(timetotal),
    timeoverhead=mean(timetotal-(timebip+timedrbip)),
    reconfigurations=mean(reconfigurations),
    n=mean(n),
    imr=mean(imr),
    rr=mean(rr),
    ir=mean(ir),
    max_conditions=mean(max_conditions),
    max_motifs=mean(max_motifs),
    motifsc=mean(motifs_created),
    motifsd=mean(motifs_destroyed),
    compsd=mean(comps_destroyed),
    compsc=mean(comps_created)
  )


temp <- res %>% filter(dynamic==1, cars > 10) %>%
  mutate(DRBIP=timedrbip, BIP=timebip + timeoverhead, RL=reconfigurations, M=max_motifs) %>% ungroup() %>%
  select(freq, dist, cars,RL, M, BIP, DRBIP)
print(xtable(temp), file="benchtable.tex")
temp


timebar <- function(f, model, d, slog = TRUE, brk=waiver()) {

  cframe = res %>% filter(freq==f, dynamic==model && dist==d) %>%
    mutate(BIP=timebip + timeoverhead)%>% ungroup()  %>%
    select(cars, BIP, timedrbip)  %>%
    rename("DR-BIP"=timedrbip)  %>%
    as_data_frame()


  cframe = melt(cframe, id.var = "cars")

  cframe$variable <- factor(cframe$variable,
                            levels=c("DR-BIP", "BIP", "Overhead"),
                            labels=c("DR-BIP", "BIP", "Overhead"))
  if(slog) vl = "log-scale"
  else vl = ""

  p = gdraw(ggplot(data=cframe,  aes_string(x="factor(cars)", y="value", fill="variable"))) +
    ylab(paste("Time (s)", vl, sep=" ")) + xlab("Cars") +
    guides(fill=guide_legend(title="Layer"))+
    scale_fill_brewer(palette = cols)
   # ggtitle(paste(f,d, sep=", "))

#  if(slog) {
    p = p + scale_y_continuous(trans="log1p",breaks=brk)+
      geom_bar(
        stat = "identity", position = position_dodge()
      )
#  }
#  else {
#    p = p + scale_y_continuous()+
#      geom_bar(
#        stat = "identity", position = position_stack()
#      )
#  }
  list("plot" = p, "frame" = cframe)
}



#x = timebar("high", 1, "0.5f", FALSE,  c(0.1, 0.5, 1,1.5,2, 2.5))
#x$plot + theme(legend.position=c(0.1,0.94), legend.direction = "vertical") + ggsave("perf-s1.pdf")
#xtable(x$frame)
#x$frame
#x = timebar("high", 1, "1.0f", TRUE, c(0.2, 1,2,5,10,20,50,100,150))
#x$plot + theme(legend.position="none")   + ggsave("perf-s2.pdf")
#xtable(x$frame)
#x$frame
#timebar("low", 1, "0.5f", TRUE)$plot
#timebar("low", 1, "1.0f", TRUE)$plot



gdraw(ggplot(data=res %>% filter(dynamic==1),  aes_string(x="ir/reconfigurations", y="timebip"))) +
  geom_line(aes(color=dist))+
  geom_point(aes(shape=dist)) + facet_grid(reformulate("factor(freq)"), scales="free", switch="both")

gdraw(ggplot(data=res %>% filter(dynamic==1),  aes_string(x="max_conditions", y="timeoverhead"))) +
  geom_line(aes(color=dist))+
  geom_point(aes(shape=dist)) + facet_grid(reformulate("factor(freq)"), scales="free", switch="both")

gdraw(ggplot(data=res %>% filter(dynamic==1),  aes_string(x="cars", y="timedrbip"))) +
  geom_line(aes(color=dist))+
  geom_point(aes(shape=dist)) + facet_grid(reformulate("factor(freq)"), scales="free", switch="both")

gdraw(ggplot(data=res %>% filter(dynamic==1),  aes_string(x="n/reconfigurations", y="timedrbip"))) +
  geom_line(aes(color=dist))+
  geom_point(aes(shape=dist))+ scale_y_continuous(trans="log1p")  + facet_grid(reformulate("factor(cars)"), scales="free", switch="both")

gdraw(ggplot(data=res %>% filter(dynamic==1, cars > 100),  aes_string(x="n/reconfigurations", y="timetotal"))) +
  geom_line(aes_string(color="factor(cars)", linetype="factor(cars)"))+
  geom_point(aes_string(shape="factor(cars)"))+
  scale_y_continuous(trans="log1p", breaks=c(0.5, 1,2,4,10,25,50,100,150)) +
  ylab("Total Execution Time (s) log-scale") +
  xlab("Average interactions executed before reconfiguring") +
  labs(color="Cars", linetype="Cars", shape="Cars") +
  theme(legend.position=c(0.85,0.98), legend.direction = "horizontal") +
  ggsave("reconf-load.pdf")

gdraw(ggplot(data=res %>% group_by(max_motifs) %>%filter(dynamic==1, cars > 100),  aes_string(x="(max_motifs)", y="timetotal"))) +
  geom_line(aes_string(color="factor(cars)", linetype="factor(cars)"))+
  geom_point(aes_string(shape="factor(cars)"))+
  scale_y_continuous(trans="log1p", breaks=c(0.5, 1,2,4,10,25,50,100,150)) +
  ylab("Total Execution Time (s) log-scale") +
  xlab("Max number of motifs observed when reconfiguring") +
  labs(color="Cars", linetype="Cars", shape="Cars") +
  theme(legend.position=c(0.85,0.98), legend.direction = "horizontal") +
  ggsave("reconf-motifs.pdf")



cframe = res %>% filter(freq=="high", dynamic==1, cars>10) %>%
  mutate(BIP=timebip + timeoverhead)%>% ungroup()  %>%
  select(cars, dist, BIP, timedrbip)  %>%
  rename("DR-BIP"=timedrbip)  %>%
  as_data_frame()

cframe = melt(cframe, id.var = c("cars","dist"))

cframe$variable <- factor(cframe$variable,
                          levels=c("DR-BIP","BIP"),
                          labels=c("DR-BIP","BIP"))
cframe$dist <- factor(cframe$dist,
                          levels=c("0.5f", "1.0f"),
                          labels=c("S1", "S2"))


gdraw(ggplot(data=cframe,  aes_string(x="factor(cars)", y="value", fill="variable"))) +
  geom_bar( stat = "identity", position = position_dodge2(), width=0.5)+
  ylab("Total Execution Time (s) log-scale") + xlab("Cars") +
  guides(fill=guide_legend(title="Layer"))+
  scale_fill_brewer(palette = cols)+
  scale_y_continuous(trans="log1p",breaks=c(0.5, 1,2,4,10,25,50,100,150))+
  facet_grid(reformulate("dist"), scales="fixed", switch="y") +
  theme(legend.position=c(0.1,0.94), legend.direction = "vertical") +
  ggsave("perf.pdf")
