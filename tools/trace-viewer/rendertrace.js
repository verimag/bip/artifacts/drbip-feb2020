function generateHslaColors (saturation, lightness, alpha, amount) {
  let colors = []
  let huedelta = Math.trunc(360 / amount)

  for (let i = 0; i < amount; i++) {
    let hue = i * huedelta
    colors.push(`hsla(${hue},${saturation}%,${lightness}%,${alpha})`)
  }

  return colors
}


var activeSnapshot = -1;
var activeState    = -1;

var mainsvg;
var root;
var layer_bar;
var layer_effect;

var spacing   = {x: 5, y: 25};
var margins   = {top: 50, left: 20, bottom: 30};
var lbl_left  = 125;
var y;
var x;

function makeLink(ar) {
  if(ar.length == 1) return [ar[0], ar[0]];
  else return ar;
}


function init(data) {
  mainsvg = d3.select("#my-canvas svg");
  root    = mainsvg.select("g");


  zoom = d3.zoom();

  zoom.scaleExtent([0.5, 2]);

  zoom.on("zoom", function () {
   root.attr("transform", d3.event.transform);
  });

  root.call(zoom);
  resetViewport();


  var rules = data.rules
                .map(function(a) {return a;})
                .flat();

  rules.unshift("");
  rules.unshift("Global");


  // Y axis
  y = d3.scaleBand()
    .range([-2 * spacing.y, rules.length * spacing.y])
    .domain(rules)
    .padding(.1);
  root.append("g")
    .call(d3.axisLeft(y));


  // Add X axis
  x = d3.scaleLinear()
    .domain([0, data.steps + 1])
    .range([20, (data.steps + 1 )* spacing.x]);

  root.append("g")
    //.attr("transform", "translate(" + margins.left +", "+ margins.top +")")
    //.call(d3.axisTop(x).ticks(data.steps));
    .call(d3.axisTop(x).ticks(Math.floor(data.steps*0.01)));

  d3.select("#svg-canvas")
    .attr("height", (margins.top + rules.length * spacing.y + margins.bottom) * 2 + "px");


  layer_bar    = root.append("g").attr("id", "l_bar");

  axis_area    = root.append("g").attr("id", "l_axis");

  var lRange = [];

  var last = null;
  for(i in data.rules) {
    var s =  data.rules[i].length;

    if(s > 0)
      last = data.rules[i][0];

    lRange.push([s + 1, last, parseInt(i), s > 0 ? data.rules[i][s-1] : ""]);
    last = data.rules[i][s-1];
  }


  axis_area.selectAll("myRect")
    .data(lRange)
    .enter()
    .append("rect")
    .attr("x", -lbl_left-5)
    .attr("y", function(d) {return y(d[1]);})
    .attr("width",  lbl_left)
    .attr("height", function(d){
      if(d[3] == "") {return 0;}
      if(d[0] > 0)
        return y(d[3]) - y(d[1]) + y.bandwidth();
      else
        return y.bandwidth();
    })
    .attr("fill", function(d){ return rule_colors[d[2]]; })
    .style("opacity", "0.1");

}

function init_heat(data) {
  mainsvg = d3.select("#my-canvas svg");
  root    = mainsvg.select("g");


  zoom = d3.zoom();

  zoom.scaleExtent([0.5, 2]);

  zoom.on("zoom", function () {
   root.attr("transform", d3.event.transform);
  });

  root.call(zoom);
  resetViewport();


  var rules = data.rules
                .map(function(a) {return a;})
                .flat();

  rules.unshift("");
  rules.unshift("Global");


  // Y axis
  y = d3.scaleBand()
    .range([(-2 * spacing.y), rules.length * spacing.y])
    .domain(rules)
    .padding(.1);
  root.append("g")
    .call(d3.axisLeft(y))


  // Add X axis
  /*
  x = d3.scaleLinear()
    .domain([0, data.switches.length])
    .range([20, (data.switches.length)* spacing.x]);
    */

  // Add X axis
  x = d3.scaleLinear()
    .domain([0, data.steps + 1])
    .range([20, (data.steps + 1 )* spacing.x]);

  root.append("g")
    //.attr("transform", "translate(" + margins.left +", "+ margins.top +")")
    .call(d3.axisTop(x).ticks(data.switches.length));

  d3.select("#svg-canvas")
    .attr("height", (margins.top + rules.length * spacing.y + margins.bottom) * 2 + "px");

  layer_bar    = root.append("g").attr("id", "l_bar");


    axis_area    = root.append("g").attr("id", "l_axis");

    var lRange = [];

    var last = null;
    for(i in data.rules) {
      var s =  data.rules[i].length;

      if(s > 0)
        last = data.rules[i][0];

      lRange.push([s, last, parseInt(i), s > 0 ? data.rules[i][s-1] : ""]);
      last = data.rules[i][s-1];
    }

    axis_area.selectAll("myRect")
      .data(lRange)
      .enter()
      .append("rect")
      .attr("x", -lbl_left-5)
      .attr("y", function(d) {return y(d[1]);})
      .attr("width",  lbl_left)
      .attr("height", function(d){
        if(d[0] > 0)
          return y(d[3]) - y(d[1]) + y.bandwidth();
        else
          return y.bandwidth();
      })
      .attr("fill", function(d){ return rule_colors[d[2]]; })
      .style("opacity", "0.1");


}
function typename(id) {
  var s = id.split("_");
  return s[0];
}
//var rule_colors = ["hsl(90, 40%, 60%)", "hsl(90, 80%, 20%)", "hsl(270, 100%, 50%)", "hsl(30, 100%, 50%)"];
var rule_colors = ["hsl(119, 100%, 30%)", "hsl(90, 100%, 20%)", "hsl(90, 40%, 60%)", "hsl(15, 100%, 40%)"];

function rcolor(rarray, i) {
  var chunk = 0;
  var nelem = 0;
  while(i > rarray[chunk].length + nelem - 1) {
    nelem += rarray[chunk].length;
    chunk++;
  }
  return rule_colors[chunk];
}


var last_click = null;

function plot_heat(mydata) {


  // create a tooltip
  var Tooltip = d3.select("#my-canvas")
    .append("div")
    .style("opacity", 0)
    .attr("class", "tooltip")
    .on("click", function(){
        Tooltip
          .style("display", "none")
          .style("opacity", 0);
        if(last_click != null) {
          last_click
            .style("stroke", "none")
            .style("opacity", 0.8);
          last_click = null;
        }
    });

  // Three function that change the tooltip when user hover / move / leave a cell

  var mouseover = function(d) {
    Tooltip.style("opacity", 1)
  }
  var mousemove = function(d) {
    Tooltip
      .html("Executions: " + d.v + "<br/>" + (d.v < 10 ? "<div class=\"heat-rules\"><ul><li>" + d.rules.join("</li><li>") + "</li></ul></div>" : ""))
      .style("left", (d3.event.pageX + 10) + "px")
      .style("top", (d3.event.pageY - 10) + "px");
  }
  var mouseleave = function(d) {
    Tooltip.style("opacity", 0)
  }


  var heatData = [];
  var totPerPhase = [];
  var maximalPerPhase = 0;
  var total = 0;

  for(i = 0; i < mydata.switches.length; i++) {
    var phase = mydata.switches[i];
    var next;
    if(i != mydata.switches.length - 1)
      next = mydata.switches[i + 1].at -1;
    else
      next = mydata.steps;


    var rules = {};
    totPerPhase[i] = 0;


    for(j = phase.at; j <= next; j++) {
      var seq = data.sequence[j];
      var t = typename(seq.name);
      total++;
      totPerPhase[i]++;
      if(typeof(rules[t]) == "undefined") {
        rules[t] = [];
      }
      rules[t].push(seq.name);
    }
    for(j in rules) {
      heatData.push({"x": i, "y" : j, "v": rules[j].length, "rules" : rules[j]});
    }
    if(totPerPhase[i] > maximalPerPhase)
      maximalPerPhase = totPerPhase[i];
  }


  //Bars
  layer_bar.selectAll("myRect")
    .data(heatData)
    .enter()
    .append("rect")
    .attr("x", function(d) { return x(d.x); } )
    .attr("y", function(d)   { return y(d.y); })
    .attr("width",  spacing.x)
    .attr("height", y.bandwidth() )
    .attr("fill",function(d) {return  rcolor(mydata.rules, Math.floor(y(d.y) / y.step()));})
    .style("opacity", function(d) {
      var v =(d.v * 1.0) / maximalPerPhase;
      return v;
    })
    .on("mouseover", mouseover)
    .on("mousemove", mousemove)
    .on("mouseleave", mouseleave);


    var switches = mydata.switches;

    layer_bar.selectAll("myRect")
      .data(switches)
      .enter()
      .append("rect")
      .attr("x", function(d,i) {
        return x(i);
      } )
      .attr("y", function(d)   { return y("Global"); })
      .attr("width", function(d,i) {return x(i+1) - x(i);})
      .attr("height", y.bandwidth() )
      .attr("fill", function(d, i){
        return (d.type == "DRBIP") ? "green" : "red";
      })
      .style("cursor", function(d,i) {
        return (d.type == "DRBIP") ? "pointer" : "arrow";
      })
      .style("opacity", 0.2)
      .on("click", function(d,i){
        if(d.type == "DRBIP") {
          showSnapshot(i);
        }
        else if(d.type == "BIP") {
          showModelState(i+1);
        }
      });

}



function plot_rseq(mydata) {


  // create a tooltip
  var Tooltip = d3.select("#my-canvas")
    .append("div")
    .style("opacity", 0)
    .attr("class", "tooltip")
    .on("click", function(){
        Tooltip
          .style("display", "none")
          .style("opacity", 0);
        if(last_click != null) {
          last_click
            .style("stroke", "none")
            .style("opacity", 0.8);
          last_click = null;
        }
    });

  // Three function that change the tooltip when user hover / move / leave a cell

  var onclick = function(d) {
    if(last_click != null) {
      last_click
        .style("stroke", "none")
        .style("opacity", 0.8)
    }
    last_click = d3.select(this);

    Tooltip
      .style("opacity", 1)
      .style("display", "block")
    d3.select(this)
      .style("stroke", "black")
      .style("opacity", 1)

    var s = "";
    if(Object.keys(d).length > 1) {
      var hasgraph = false;
      s = "<table class=\"rule-details\">";
      for(var key in d) {
        if(key == "name") continue;

        if(key == "interaction") {
          hasgraph = true;
          continue;
        }
        s += "<tr><th>" + key + ":</th>";
        s += "<td>" + d[key] + "</td></tr>";typename(d.name)
      };
      s += "</table>";
      if(hasgraph) {
        s+= "<div class=\"rule-interaction\">" + d.interaction.name + "</div>";
        s+= "<div id=\"rule-graph\"></div>";
      }
    }

    Tooltip
      .html("<div class=\"rule-name\">" + d.name + "</div>" + s)
      //.style("left", (d3.mouse(this)[0] +70) + "px")
      //.style("top", (d3.mouse(this)[1]) + "px");
      .style("left", (d3.event.pageX + 10) + "px")
      .style("top", (d3.event.pageY - 10) + "px");

      if(hasgraph) {
        var myComps = d.interaction.comps.flat();

        var myLinks = [myComps];
        if(myComps.length == 1) {
          myLinks = [[myComps[0], myComps[0]]];
        }
        var ngraph = {"nodes":[], "links" : myLinks};

        for(myNode in myComps){
          ngraph.nodes.push({"id" : myComps[myNode]});
        }

        makeGraph('rule-graph', ngraph);

      }

  }

  //Bars
  layer_bar.selectAll("myRect")
    .data(mydata.sequence)
    .enter()
    .append("rect")
    .attr("x", function(d,i) { return x(i); } )
    .attr("y", function(d)   { return y(typename(d.name)); })
    .attr("width",  spacing.x)
    .attr("height", y.bandwidth() )
    .attr("fill",function(d,i) { return  rcolor(mydata.rules, Math.floor(y(typename(d.name)) / y.step())); })
    .style("opacity", 0.8)
    .on("click", onclick);

    var switches = mydata.switches;

    layer_bar.selectAll("myRect")
      .data(switches)
      .enter()
      .append("rect")
      .attr("x", function(d,i) {
        return x(d.start);
      } )
      .attr("y", function(d)   { return y("Global"); })
      .attr("width",  function(d,i) {
        return x(d.end + 1) - x(d.start);
      })
      .attr("height", y.bandwidth() )
      .attr("fill", function(d, i){
        return (d.type == "DRBIP") ? "green" : "red";
      })
      .style("cursor", function(d,i) {
        //return (d.type == "DRBIP") ? "pointer" : "arrow";
        return "pointer";
      })
      .style("opacity", 0.2)
      .on("click", function(d,i){
        if(d.type == "DRBIP") {
          showSnapshot(i);
        }
        else if(d.type == "BIP") {
          showModelState(i+1);
        }
      })
}


var MVT_SENS = 3000;


function moveGraph() {
  var x = 0;
  var y = 0;
  switch(d3.event.keyCode) {
    case 83: //s
      //d3.selectAll('.panel').style('display', 'none');
      loadData(data);
      break;
    case 72: //h
      //d3.selectAll('.panel').style('display', 'none');
      heatPlot(data);
      break;
    case 38: //ArrowUp
    case 87: //W
      y += Math.round(root.node().getBBox().height * 0.2);
      break;
    case 40: //ArrowDown
    case 83: //S
      y -= Math.round(root.node().getBBox().height * 0.2);
      break;
    case 37: //ArrowLeft
    case 65: //A
      x += Math.round(root.node().getBBox().width * 0.1);
      break;
    case 39: //ArrowRight
    case 68: //D
      x -= Math.round(root.node().getBBox().width * 0.1);
      break;
    //case 88:
    //	test();
  }

  if(x != 0 | y != 0) {

      root.transition().duration(2500).call(zoom.translateBy, x, y);
  }
}
function moveSnapshots() {
  var to = -1;

  switch(d3.event.keyCode) {
    case 38: //ArrowUp
    case 87: //W
      break;
    case 40: //ArrowDown
    case 83: //S
      break;
    case 37: //ArrowLeft
    case 65: //A
      to = activeSnapshot + -1;
      while(to >= 1 && data.switches[to].type != "DRBIP") {
        to--;
      }
      break;
    case 39: //ArrowRight
    case 68: //D
      to = activeSnapshot + 1;
      while(to < data.switches.length && data.switches[to].type != "DRBIP") {
        to++;
      }
      break;
    //case 88:
    //	test();
  }
  if(to != -1)
    showSnapshot(to);


}


function moveStates() {
  var to = -1;



  switch(d3.event.keyCode) {
    case 38: //ArrowUp
    case 87: //W
      break;
    case 40: //ArrowDown
    case 83: //S
      break;
    case 37: //ArrowLeft
    case 65: //A
      to =activeState - 1;
      while(to >= 0 && data.switches[to].type != "DRBIP") {
        to--;
      }
      break;
    case 39: //ArrowRight
    case 68: //D
      to = activeState + 1;
      while(to < data.switches.length && data.switches[to].type != "DRBIP") {
        to++;
      }
      break;
    //case 88:
    //	test();
  }
  if(to != -1)
    showModelState(to);

}

d3.select("body").on("keydown", function () {
  if(d3.event.keyCode == 80 && data != null) {
    customPlot();
  }
  if(activeSnapshot != -1) {
    moveSnapshots();
  }
  else if(activeState != -1) {
    moveStates();
  }
  else {
    moveGraph();
  }
});

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
function box() {
		return d3.select("#demo svg").node().getBBox();
}

d3.select("#my-canvas").on("dblclick", function(){
  //root.attr("transform", "translate(0,0) scale(1)");
  resetViewport();
});

function resetViewport(){
  //root.transition().duration(1050).call(zoom.transform, d3.zoomIdentity);
  //root.transition().duration(700).call(zoom.translateBy,, margins.top + spacing.y * 2.2);
  var x =  margins.left + lbl_left;
  var y =  margins.top + spacing.y * 2;
  //root.attr("transform", "translate("+ x +", "+ y +") scale(1)");


  root.call(zoom.transform, d3.zoomIdentity);
  root.transition().duration(700).call(zoom.translateBy,x,y);
}


function showSnapshot(id) {
  if(typeof(data.switches[id]) != "undefined") {
    if(id < 0) return;
    if(data.switches[id].type != "DRBIP") return;

    d3.select("#my-canvas").style("display", "none");
    d3.select("#my-snapshot").style("display", "block").html("");

    activeSnapshot = id;
    activeState    = -1;
    var snap = d3.selectAll("#my-snapshot");

    snap.append("div").classed("snapshot-label", true).html("Snapshot#" + id);


    var phase = data.switches[id];

    if(phase.snapshot.motifs.length == 0) return;

    var myMotifs = {};
    for(m  in phase.snapshot.motifs) {
      var mdata = phase.snapshot.motifs[m];
      myMotifs[mdata.motif] = {
        "name" : mdata.motif,
        "comps" : mdata.comps,
        "links" : [],
        "linksData" : [],
        "itypes": {},
      };
    }
    lower = data.switches[id].start;
    upper = data.switches[id].end;


    for(i = lower; i <= upper; i++) {
      var sdata = data.sequence[i];

      if(typeof(sdata["interaction"]) == "undefined") continue;
      if(typeof(sdata["motif"]) == "undefined") continue;


      if(myMotifs[sdata.motif] == "undefined")
      {
        console.log("Undefined motif: " + sdata.motif);
        continue;
      }



      myMotifs[sdata.motif].links.push(makeLink(sdata.interaction.comps.flat()));
      myMotifs[sdata.motif].linksData.push({
          "interaction" : sdata.interaction.name
        , "rule"        : sdata.name
      });
      myMotifs[sdata.motif].itypes[typename(sdata.interaction.name)] = 1;
    }




    for(motif in myMotifs) {
      var block = snap.append("div");
      block.attr("class", "motif-block");
      block.on("click", function(){
        d3.event.stopPropagation();
      });
      var motiftitle = block.append("div");
      motiftitle.classed("snapshot-motif-title", true);

      motiftitle.html(motif);

      var motifgraph = block.append("div");
      motifgraph.attr("id", "graph-" + motif);
      motifgraph.classed("snapshot-motif-graph", true);

      var colors = {};
      var istep = 0;
      for(type in  myMotifs[motif].itypes) {
        colors[type] = istep;
        istep++;
      }

      var gradient = generateHslaColors(50, 50, 1.0, istep);


      makeGraph("graph-" + motif, {
        "nodes": myMotifs[motif].comps.map(function(d){return {"id" : d}}),
        "links": myMotifs[motif].links
      }, function(d,idx,node) {
        var node = d3.select(node[idx]);
        var i = d[3];


        //if(i >= myMotifs[motif].linksData.length) return;
        node.style("stroke", gradient[colors[typename(myMotifs[motif].linksData[i].interaction)]]);


        node.append("title").text(
          myMotifs[motif].linksData[i].interaction
          + " (" + myMotifs[motif].linksData[i].rule + ")");


      });
    }
    snap.append("div").attr("style", "clear:both");
  }
  else {
    //console.log("No snapshot for: " + id);
  }


}

function showModelState(id) {

  if(typeof(data.switches[id]) != "undefined") {
    if(data.switches[id].type != "DRBIP") return;
    d3.select("#my-canvas").style("display", "none");
    d3.select("#my-snapshot").style("display", "none").html("");
    d3.select("#my-state").style("display", "block").html("");

    activeState = id;
    activeSnapshot = -1;

    var phase = data.switches[id];
    modelinfo.draw(phase.snapshot);
  }
  else {
    //console.log("No snapshot for: " + id);
  }
}
d3.select('#my-state').on("click", function(){
  d3.select("#my-canvas").style("display", "block");
  d3.select("#my-state").style('display', 'none').html("");
  activeState = -1;
});

d3.select("#my-snapshot").on("click", function(){
  d3.select("#my-canvas").style("display", "block");
  d3.select("#my-snapshot").style("display", "none").html("");
  activeSnapshot = -1;
});

var zoom;

function clear_graph() {
  d3.select('#interaction-graph').html("");
  d3.select('#my-snapshot').html("");
  d3.select('#svg-canvas g').html("");
}
function loadData(d) {
    data = d;
    clear_graph();
    init(d);
    plot_rseq(d);
    resetViewport();
}

function heatPlot(d) {
  data = d;
  clear_graph();
  init_heat(d);
  plot_heat(d);
  resetViewport();
}

/*
init(data);
plot_rseq(data);
*/

/*
d3.json("platoon.json", function(error, data) {
  console.log(error);
  console.log(data);
  //init(data);
  //plot_rseq(data);
});*/
