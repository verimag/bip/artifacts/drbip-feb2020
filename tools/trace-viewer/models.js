var models = {
    "" : {
      score_info : {'n' : 0, 'labels' : [], 'colors' : []},
      name : "",
      score: function(snapshot) {return [];},
      draw : function(snapshot) {},
      post : function(data){}
    },
    "platoon" : {
    name : "platoon",
    score_info : {
      'n' : 3,
      'labels' : ['Uniform Separation', 'Target Size', 'Road Occupancy'],
      'colors' : ['steelblue', 'orange', 'purple']
    },
    draw : function(snapshot) {

        var comps = {};
        var min = 1000;
        var max = 0;

        var gtail;
        var ghead;

        var c_middle = snapshot.components[0];

        nmiddle = (snapshot.components.length / 2) - 1;

        var head, tail, prev_tail;

        var platoons = [];

        for(c in snapshot.components) {
          var myComp = snapshot.components[c];
          var obj = {
            "name"    : myComp.component,
            "platoon" : "n/a",
            "pos"   : myComp.state._m__pos,
            "v"     : myComp.state._m__v,
            "color" : "black"
            //,"loc"   : myComp.state._loc
          };

          comps[myComp.component] = obj;

          if(myComp.state._m__pos < min)
            min = myComp.state._m__pos;
          if(myComp.state._m__pos > max)
            max = myComp.state._m__pos;
          if(myComp.component.endsWith("_" + nmiddle)) {
            c_middle = obj;
          }
        }

        car_gradient = generateHslaColors(50, 50, 1.0, snapshot.motifs.length);

        for(m in snapshot.motifs) {
          var myMotif = snapshot.motifs[m];
          if(myMotif.motif.startsWith('Platoon')) {
            head = null;
            tail = null;
            for(cc in myMotif.comps) {
              var car = myMotif.comps[cc];
              var myCar = comps[car];
              myCar.platoon = myMotif.motif;
              myCar.color   = car_gradient[m];

              if(head == null) head = myCar;
              else if(head.pos < myCar.pos) head = myCar;

              if(tail == null) tail = myCar;
              else if(myCar.pos <tail.pos) tail = myCar;

              if(ghead == null) ghead = myCar;
              else if(ghead.pos < myCar.pos) ghead = myCar;
              if(gtail == null) gtail = myCar;
              else if(myCar.pos <gtail.pos) gtail = myCar;
            }
            platoons.push({
               "platoon" : myMotif.motif,
               "head"    : head,
               "tail"    : tail,
               "relpos"  : (head.pos + tail.pos)/2,
               "size"    : myMotif.comps.length
            });
          }
        }

        platoons = platoons.sort(function(a,b){return b.relpos - a.relpos});

        ssvg = d3.select("#my-state").append("svg");
        sroot= ssvg.append("g");


        ssvg.attr('width', '100%');
        ssvg.attr('height', '100%');
        ssvg.attr('overflow', 'scroll');

        var pbox = getPbox();

        x = d3.scaleLinear()
                .domain([min - 1, max + 1])
                .range([pbox.left, pbox.right / 2]);



        sroot.append("g")
              .attr("transform", "translate(0," + pbox.top + ")")
              .call(d3.axisBottom(x));


        car_area     = sroot.append("g").attr("id", "l_cars");

        platoon_area = sroot.append("g").attr("id", "l_distances");

        car_area.selectAll("myRect")
          .data(snapshot.components)
          .enter()
          .append("rect")
          .attr("x", function(d) {return x(comps[d.component].pos) - 5;})
          .attr("y", pbox.top + 30)
          .attr("width",  10)
          .attr("height", 20)
          .attr("fill", function(d){ return comps[d.component].color; });



        platoon_area.selectAll("myRect")
          .data(platoons.slice(0, platoons.length - 1))
          .enter()
          .append("line")
          .attr("x1", function(d,i) {return x(d.tail.pos);})
          .attr("x2", function(d,i) {return x(platoons[i+1].head.pos);})
          .attr("y1", pbox.top + 60)
          .attr("y2", pbox.top + 60)
          .attr("stroke", "black");

        platoon_area.selectAll("myRect")
          .data(platoons.slice(0, platoons.length - 1))
          .enter()
          .append("text")
          .attr("x", function(d,i) {return (x(d.tail.pos) + x(platoons[i+1].head.pos))/2 - 5;})
          .attr("y", pbox.top+ 80)
          .attr("fill", "black")
          .attr("stroke", "1px")
          .text(function(d,i) { return myround(d.tail.pos - platoons[i+1].head.pos, 2);})
          ;

      var lheight = 100;
       platoon_area.append("line")
        .attr("x1", x(gtail.pos))
        .attr("x2", x(ghead.pos))
        .attr("y1", pbox.top + lheight)
        .attr("y2", pbox.top + lheight)
        .attr("stroke", "black");
      platoon_area.append("line")
       .attr("x1", x(gtail.pos))
       .attr("x2", x(gtail.pos))
       .attr("y1", pbox.top + lheight + 10)
       .attr("y2", pbox.top + lheight - 10)
       .attr("stroke", "black");
     platoon_area.append("line")
      .attr("x1", x(ghead.pos))
      .attr("x2", x(ghead.pos))
      .attr("y1", pbox.top + lheight + 10)
      .attr("y2", pbox.top + lheight - 10)
      .attr("stroke", "black");
      platoon_area.append("text")
       .attr("x", x((gtail.pos + ghead.pos)/2) - 10)
       .attr("y", pbox.top + lheight + 20)
       .attr("fill", "black")
       .attr("stroke", "1px")
       .text(myround(ghead.pos - gtail.pos));



     var L = ghead.pos - gtail.pos;
     var n = platoons.length;
     var d = [];
     var s = [];

     var s_int = [2, 3];
     var dsum = 0;

     for(i = 0; i < platoons.length - 1; i++) {
       var dist = Math.sqrt(Math.pow(platoons[i].tail.pos - platoons[i + 1].head.pos, 2));
       d.push(dist);
       s.push(platoons[i].size);
       dsum += dist;
     }
     if(platoons.length > 0)
      s.push(platoons[platoons.length - 1].size);

     var scorea = this.score(snapshot);

     score = scorea[0] + scorea[1] + scorea[2];

   platoon_area.append("text")
    .attr("x", x(gtail.pos) + 10)
    .attr("y", pbox.top + 150)
    .attr("fill", "black")
    .attr("stroke", "1px")
    .text("Score: " + myround(score) +
    " (US=" + myround(scorea[0]) +
    ", TS=" + myround(scorea[1]) +
    ", RO=" + myround(scorea[2]) + ")");

       console.log(comps);
    }
    , score : function(snapshot) {
          var comps = {};
          var min = 1000000;
          var max = 0;

          var gtail;
          var ghead;

          var c_middle = snapshot.components[0];

          nmiddle = (snapshot.components.length / 2) - 1;

          var head, tail, prev_tail;

          var platoons = [];

          for(c in snapshot.components) {
            var myComp = snapshot.components[c];
            var obj = {
              "name"    : myComp.component,
              "platoon" : "n/a",
              "pos"   : myComp.state._m__pos,
              "v"     : myComp.state._m__v
              //,"loc"   : myComp.state._loc
            };

            comps[myComp.component] = obj;

            if(myComp.state._m__pos < min)
              min = myComp.state._m__pos;
            if(myComp.state._m__pos > max)
              max = myComp.state._m__pos;
            if(myComp.component.endsWith("_" + nmiddle)) {
              c_middle = obj;
            }
          }

          for(m in snapshot.motifs) {
            var myMotif = snapshot.motifs[m];
            if(myMotif.motif.startsWith('Platoon')) {
              head = null;
              tail = null;
              for(cc in myMotif.comps) {
                var car = myMotif.comps[cc];
                var myCar = comps[car];
                myCar.platoon = myMotif.motif;

                if(head == null) head = myCar;
                else if(head.pos < myCar.pos) head = myCar;

                if(tail == null) tail = myCar;
                else if(myCar.pos <tail.pos) tail = myCar;

                if(ghead == null) ghead = myCar;
                else if(ghead.pos < myCar.pos) ghead = myCar;
                if(gtail == null) gtail = myCar;
                else if(myCar.pos <gtail.pos) gtail = myCar;
              }
              platoons.push({
                 "platoon" : myMotif.motif,
                 "head"    : head,
                 "tail"    : tail,
                 "relpos"  : (head.pos + tail.pos)/2,
                 "size"    : myMotif.comps.length
              });
            }
          }

       platoons = platoons.sort(function(a,b){return b.relpos - a.relpos});
       var L = ghead.pos - gtail.pos;
       var n = platoons.length;
       var d = [];
       var s = [];

       var s_int = [2, 3];
       var score_evendist = 0.0;
       var score_sizedist = 0.0;
       var score_compact  = 0.0;

       var dsum = 0;

       var maxdist = 0;
       for(i = 0; i < platoons.length - 1; i++) {
         var dist = Math.sqrt(Math.pow(platoons[i].tail.pos - platoons[i + 1].head.pos, 2));
         d.push(dist);
         s.push(platoons[i].size);
         if(dist > maxdist) maxdist = dist;
         dsum += dist;
       }
       if(platoons.length > 0)
        s.push(platoons[platoons.length - 1].size);

        //console.log("Platoon Information")
        //console.log(d);
        //console.log(s);

        var dint = maxdist > 0 ? [0, maxdist] : null;


       var dnorm = (d.length > 1) ? feature_norm(d, null, dint) : null;
       var snorm = (s.length > 0) ? feature_norm(s, [2, 2], [0, data.meta.N_cars]) : null;

       if(dnorm) score_evendist = dnorm[3];
       if(snorm) score_sizedist = snorm[3];

       if(platoons.length > 1) {
         score_compact  = 1- ((L - dsum) / L);
       }



      return [score_evendist, score_sizedist, score_compact];
    }
    , post : function(data) {
        var mind,maxd;
        for(s in data.switches) {

           if(data.switches[s].snapshot.components.length > 0){
             var snapshot = data.switches[s].snapshot;
             var d = this.dist(snapshot);

             if(mind == null) mind = d;
             else if(d < mind) mind = d;
             if(maxd == null) maxd = d;
             else if(d > maxd) maxd = d;

           }
        }
        data["meta"] = {
          "L_min" : mind,
          "L_max" : maxd,
          "N_cars": data.switches[0].snapshot.components.length
        };
    }
    , dist : function(snap) {
      var head = snap.components[0];
      var tail = snap.components[0];

      for(c in snap.components) {
        var comp = snap.components[c];
        if(comp.state._m__pos < tail.state._m__pos) tail = comp;
        if(head.state._m__pos < comp.state._m__pos) head = comp;
      }

      return Math.sqrt(Math.pow(head.state._m__pos - tail.state._m__pos,2));
    }
  }

};
