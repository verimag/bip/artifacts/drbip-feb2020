#!/bin/bash
BLOG=bench.log

FREQ=("low;500;700" "med;300;500" "high;150;200")

rm -rf $BLOG
echo "--Starting bench" > $BLOG
echo "freq,cars,dist,dynamic,n,timebip,interactions,timetotal,timedrbip,reconfigurations,imr,rr,ir,comps_created,comps_destroyed,motifs_created,motifs_destroyed,max_motifs,max_comps,max_conditions"

for fline in ${FREQ[@]}
do
IFS=';' read -ra freq <<< "$fline"
FLBL=${freq[0]}
FMIN=${freq[1]}
FMAX=${freq[2]}
for car in 10 100 500 1000
do
  for dist in "0.3f" "1.0f"
  do
     for tgl in 0 1
     do
       make clean >> $BLOG 2>> $BLOG
       make cleanmodel > $BLOG 2>> $BLOG
       echo "### cars=$car dist=$dist dyn=$tgl movemin=$FMIN movemax=$FMAX mergedist=0.5f" >> $BLOG
       make genmodel cars=$car dist=$dist dyn=$tgl movemin=$FMIN movemax=$FMAX mergedist=0.5f >> $BLOG 2>> $BLOG
       make gencode >> $BLOG 2>> $BLOG
       for n in 5001
       do
          for k in $(seq 1 $1)
          do
            res=`make bench N=$n 2> /dev/null | tail -n 15 | cut -d":" -f2 | cut -d " " -f2 | paste -s -d","`
            echo "$FLBL,$car,$dist,$tgl,$((n - 1)),$res"
          done
       done
     done
  done
done
done
echo "--End bench" > $BLOG
