#!/bin/bash
# filein - fileout - cars - dist - minmove - maxmove
cars=$(($3 - 1))
sed -e "s/%ncars%/$3/" \
    -e "s/%_carsidx%/$cars/" \
    -e "s/%idist%/$4/" \
    -e "s/%movemin%/$5/"\
    -e "s/%movemax%/$6/"\
    -e "s/%mergedist%/$7/"\
    $1 > $2
