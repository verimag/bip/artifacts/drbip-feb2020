#include<drbip.h>
#include<platoonTypes.hpp>
#include<map>
#include<list>
#include<set>
using namespace platoon;


namespace reconfigure {

    //Check AT__CAR to be in location 3 and output notification 1
    class AtLocSplit : public CondState<AT__CAR, 3, 1> {
    public:
        AtLocSplit(AT__CAR * atom) : CondState(atom) {}
    };

    class AtLocLeader : public CondState<AT__CAR, 1, 1> {
    public:
        AtLocLeader(AT__CAR * atom) : CondState(atom) {}
    };
    class AtLocFollower : public CondState<AT__CAR, 2, 1> {
    public:
        AtLocFollower(AT__CAR * atom) : CondState(atom) {}
    };

    class AtLocMerge: public CondState<AT__CAR, 4, 1> {
    public:
        AtLocMerge(AT__CAR * atom) : CondState(atom) {}
    };

    class CarProximity : public Condition {

    private:
        AT__CAR*  c1;
        AT__CAR*  c2;
        float thresh = 2.0f;
        bool lastEval = false;

    public :

        CarProximity(AT__CAR* car1, AT__CAR* car2, float d) {
          c1 = car1;
          c2 = car2;
          thresh = d;
        }

        bool getValue() override {
          return lastEval;
        }

        static bool eval(AT__CAR* car1, AT__CAR* car2, float d) {
          return (car2->_m__pos - car1->_m__pos) <= d;
        }

        bool evaluate() override {
          lastEval = eval(c1,c2,thresh);
          return lastEval;
        }

        int notifyID() override {
          return 3;
        }

        void reset() {
          lastEval = false;
        }
    };

    class MergerMap {
    public:
      Node * join;
      Node * tail;
      Node * head;

      Node * create() {
        return new Node();
      }

      void setHead(Node * head) {
        this->head = head;
      }
      void setTail(Node * tail) {
        this->tail = tail;
      }
      void setJoin(Node * join) {
        this->join = join;
      }

      Node * getHead() { return head; }

      bool isHead(Node * node) { return node == head; }
      bool isTail(Node * node) { return node == tail; }
      bool isJoin(Node * node) { return node == join; }

      ~MergerMap() {
        delete join;
        delete head;
        delete tail;
      }
    };


    class PlatoonMap {
    public:
        list<Node*> nodes;

        Node * create() {
            return new Node();
        }
        bool isLeader(Node * node) {
          return node == nodes.back();
        }
        bool isTail(Node * node) {
          return node == nodes.front();
        }

        Node * getLeader() {
          return nodes.back();
        }
        Node * getTail() {
          return nodes.front();
        }

        bool isBehind(Node * n1, Node * n2) {
          for(auto elem = nodes.begin(); elem != nodes.end(); ++elem) {
            if(*elem == n1) {
              ++elem;
              if(elem == nodes.end()) return false;
              return *elem == n2;
            }
          }
          return false;
        }

        void insert(Node * node) {
          nodes.push_front(node);
        }

        void assign(PlatoonMap * platoon) {
          nodes.clear();
          nodes = platoon->nodes;
        }

        bool validSplit(Node * node) {
          int left = 0;
          int right = nodes.size();


          for(auto elem = nodes.begin(); elem != nodes.end(); ++elem) {
            left++;
            if(*elem == node) {
              right = nodes.size() - left;
              break;
            }
          }
          return left > 1 && right > 1;

        }
        void merge(PlatoonMap & platoon) {
          #ifndef PERFMODE
          cout << "[DEBUG] [PlatoonMap] [MERGE] Begin merge" << endl;
          print();
          platoon.print();
          #endif
          for(auto elem = platoon.nodes.rbegin(); elem != platoon.nodes.rend(); ++elem) {
            insert(*elem);
          }
          #ifndef PERFMODE
          print();
          cout << "[DEBUG] [PlatoonMap] [MERGE] Complete" << endl;
          #endif;

        }
        void print() {
          cout << "[DEBUG] [PlatoonMap] [PLATOON] #" << this << ":";
          for(auto elem = nodes.begin(); elem != nodes.end(); ++elem) {
            cout << " " << *elem;
          }
          cout << " | Leader: " << getLeader();
          cout << endl;
        }

        set<PlatoonMap*>* splitAt(Node * node, bool leaving) {
          assert(validSplit(node));

          #ifndef PERFMODE
          cout <<  "[DEBUG] [PlatoonMap] Node to split at: " << node << endl;
          #endif
          set<PlatoonMap*>* res = new set<PlatoonMap*>();

          PlatoonMap* left  = new PlatoonMap();
          PlatoonMap* right = new PlatoonMap();

          bool insertLeft = true;
          #ifndef PERFMODE
          cout << "[DEBUG] [PlatoonMap] Leader: " << getLeader() << endl;
          cout << "[DEBUG] [PlatoonMap] List: ";
          #endif;

          for(auto elem = nodes.begin(); elem != nodes.end(); ++elem) {

            if(!(*elem == node && leaving)) {
              if(insertLeft) left->nodes.push_back(*elem);
              else          right->nodes.push_back(*elem);
            }

            #ifndef PERFMODE
            cout << *elem << " ";
            #endif

            if (*elem == node) {
              insertLeft = false;
            }
          }
          cout << endl;
          #ifndef PERFMODE



          cout << "[DEBUG] [PlatoonMap] Resulting split: " << left->nodes.size() << " <-> " << right->nodes.size() << endl;
          cout << "[DEBUG] [PlatoonMap] Leaders: " << left->getLeader() << " <-> " << right->getLeader() << endl;

          left->print();
          right->print();
          #endif


          res->insert(left);
          res->insert(right);

          assert(right->nodes.size() > 0);
          assert(left->nodes.size() > 0);

          return res;
        }
    };

    class PlatoonAddressing {
    public:
        map<AtomType*, Node*> nodeMap;

        void bind(AtomType* atom, Node* node) {
          nodeMap.insert(pair<AtomType*, Node*>(atom, node));
        }


        AT__CAR * findLeader(PlatoonMap & map) {
          Node * lead = map.getLeader();
          assert(lead != NULL);

          for(auto elem = nodeMap.begin(); elem != nodeMap.end(); ++elem)
            if(elem->second == lead)
              return (AT__CAR*) elem->first;

          assert(false);
          return NULL;
        }
        AT__CAR * findTail(PlatoonMap & map) {
          Node * lead = map.getTail();
          assert(lead != NULL);

          for(auto elem = nodeMap.begin(); elem != nodeMap.end(); ++elem)
            if(elem->second == lead)
              return (AT__CAR*) elem->first;

          assert(false);
          return NULL;
        }

        Node * operator()(AtomType * atom) {
          auto value = nodeMap.find(atom);
          if(value != nodeMap.end())
            return value->second;
          else
            return NULL;
        }
        void merge(PlatoonAddressing * address) {
          nodeMap = std::move(address->nodeMap);
          delete address;
        }

        void merge(PlatoonAddressing & address) {
          nodeMap.insert(address.nodeMap.begin(), address.nodeMap.end());
        }
        set<AT__CAR*>* getAllCars() {
          set<AT__CAR*>* cars = new set<AT__CAR*>();

          for(auto elem = nodeMap.begin(); elem != nodeMap.end(); ++elem)
            cars->insert((AT__CAR*) elem->first);

          return cars;
        }
        PlatoonAddressing* slice(PlatoonMap* list) {

          PlatoonAddressing * res = new PlatoonAddressing();

          for(auto node = list->nodes.begin(); node != list->nodes.end(); ++node) {
            for(auto map = nodeMap.begin(); map != nodeMap.end(); ++map) {
              if(map->second == *node)
                res->bind(map->first, *node);
            }
          }


          return res;
        }
    };


}
