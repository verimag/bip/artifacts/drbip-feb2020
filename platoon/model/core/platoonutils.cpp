#ifndef PLATOONUTILS
#define PLATOONUTILS 1

#include<cstdlib>
#include<iostream>
using namespace std;

const float MAX_SPEED = 0.25f;
const float MIN_SPEED = 0.15f;
const float DELTA_INC = 1.02f;
const float DELTA_DEC = 0.98f;

const int IDLE_MOVE = %movemin%;
const int IDLE_MIN  = %movemax%;


float mergeDist() {
  return %mergedist%;
}

int getRandomNumber(int min, int max)
{
    static constexpr double fraction { 1.0 / (RAND_MAX + 1.0) }; 
    return min + static_cast<int>((max - min + 1) * (std::rand() * fraction));
}

int smoothMove(int x) {
  return getRandomNumber(IDLE_MIN, (IDLE_MIN) + IDLE_MOVE);
}
int updateStep(int step) {
  if(step > 0) return step - 1;
  else return 0;
}
float speedLead(float speed) {
  float sp2 = speed * DELTA_INC;
  float res;
  if(sp2 > MAX_SPEED) res = MAX_SPEED;
  else                res = sp2;
  return res;
}
float speedSplit(float speed) {
  float sp2 = speed * DELTA_DEC;
  float res;
  if(sp2 < MIN_SPEED) res = MIN_SPEED;
  else                res = sp2;
  return res;
}
#endif
