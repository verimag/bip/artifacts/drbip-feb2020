## Platoon System

In this example, we have a single lane road (highway) with multiple cars. Cars organize in platoons. For each platoon there is a leader car which governs the speed of the entire platoon.

All cars initially form a single platoon. A single car can non-deterministically choose to split from the platoon, forming its own platoon.

Platoons which are close merge together to form a single platoon.

Examples are described in details in the [ISOLA'18 paper:](https://www-verimag.imag.fr/~sifakis/papers_pdfs/isola18.pdf)

> Rim El Ballouli, Saddek Bensalem, Marius Bozga, Joseph Sifakis:
*Four Exercises in Programming Dynamic Reconfigurable Systems: Methodology and Solution in DR-BIP*. ISoLA (3) 2018: 304-320

### Instantiating the Platoon System model

The DR-BIP model is composed of:

1. A `.bip` model is provided to provide a blueprint of the static BIP model, including atomic components and connectors.
1. A `.drbip` model that includes the relevant motifs, interaction, reconfiguration and global inter-motif rules.
1. A `.cpp` file which include conditions, structures (maps) and addressing function implementations for usage with motifs.

The Platoon System model can be found in [model/core](model/core) and its' parameters are instantiated to fit our execution.

To instantiate a test model perform:

```
make gentestmodel
```

The parameters of the Platoon System are as follows:

* `dyn` : 0 to disable splits (disables reconfiguration), 1 to enable splits;
* `cars` : fixed number of cars on the road;
* `dist` : initial distance between cars (in float, i.e.: *0.5f*), the merge distance is *0.5f*;
* `movemin`-`movemax`: an interval specifying the possible number of steps before a car can merge (regulating reconfiguration frequency), each follower is assigned a number between these two; and
* `mergedist` : the distance at which platoons are merged.


To instantiate a model with the specific parameters you must provide a value for **ALL** parameters:

```
make genmodel dyn=1 cars=100 dist=1.0f movemin=150 movemax=250 mergedist=0.5f
```

> **Note:** the `cleanmodel` target can be used to remove all model created files as they are normally not removed when performing a clean.

### Executing the Platoon System

With the model instantiated, we now proceed to compile and execute it.

#### Code generation

To generate the C++ files by compiling both BIP and DR-BIP files execute:

```
make gencode
```

The code generation works using 2 sub-phases.

1. First, the BIP compiler is used to generate the BIP engine and types from the `.bip` file (target: `genbip`).
1. Second, the DR-BIP compiler is used to generate the DR-BIP engine and copy the relevant files (target: `gendrbip`).

#### Code Compilation and Execution

Executing the system will run the system until it deadlocks (note that the Platoon System never deadlocks, so it runs indefinitely). To execute it use:

```
make execute
```

To limit the execution to a specific amount of BIP interactions use:

It may be preferred to limit the BIP system execution if so simply using the `limit` target with the `N` parameter to specify the cutoff.
For example, to limit the execution to 15, use:

```
make limit N=15
```

#### Tracing

If you wish to generate execution traces (of length `N` BIP interactions) the processing is automatically done using:

```
make trace N=1000
```

This outputs a new `.json` file with structured information about the trace.

To view all traces (if you have Python3) it is possible to use the [web tool](../tools/trace-viewer) automatically using:

```
make viewtraces
```

Then opening a browser on: [http://0.0.0.0:8000](http://0.0.0.0:8000).

The score graph is shown first, by clicking on it, it closes allowing you to explore the other graphs:

* Press `s` to view the action trace -- sequence of actions applied. Clicking on a red bar on top displays the global view, while clicking on a green bar displays the reconfiguration view of the system (after the reconfiguration). You can use arrows to navigate snapshots. Clicking on interaction rules displays the created connector.
* Press `h` to view a heat map view of the action trace.
* Press `p` to view the score plot again.


A set of example traces is provided with the [web tool](../tools/trace-viewer/).
To view them go to the folder:

```
cd ../tools/trace-viewer
```

And run `make`.

**Replicating the Scenarios from the Paper**

To replicate the traces provided in the paper for the two scenarios, proceed as follows.

*Scenario 1*: Cars are initially separated by less than the merge distance.

```
make genmodel dyn=1 cars=1000 dist=0.3f movemin=150 movemax=200 mergedist=0.5f
make gencode
make trace N=25000 TPREFIX=S1-
```

*Scenario 2*: Cars are initially separated by twice the merge distance.

```
make genmodel dyn=1 cars=1000 dist=1.0f movemin=150 movemax=200 mergedist=0.5f
make gencode
make trace N=3000 TPREFIX=S2-
```

You can now view both traces with:

```
make viewtraces
```

#### Performance Analysis

The system can be run in performance mode, hiding all print statements and logging information in counters that are printed at the end of the execution stopping after `N` BIP executions.

```
make bench N=1000
```

A full benchmark script ([bench.sh](bench.sh)) is provided to execute benchmarking for various number of parameters and outputs CSV data.

To execute the benchmark script (which may take **30-60** minutes) you need to provide a number of executions per combinations of parameters, and you can optionally use `tee` to store the output in a file:

```
./bench.sh 10 | tee bench.csv
```

The generated CSV file can be analyzed and plotted in [../plot](../plot).

### General Implementation Details when Writing DR-BIP models

> **Note:** This section is intended for those wanting to write their own models or rules.

The used version of the BIP compiler is built around the new-glue branch and as such has certain restrictions when inferring types from the expressions in the BIP model. Further restrictions are due to design decisions for DR-BIP.  They are as follows:

1. Connectors are parametrized by *atomic components* and not *ports*. The generated code will utilize **only** the first declared instance in the compound component to infer ports. If the same components use different ports then it is required to generate different types.
1. Due to the new-glue being based on expressions, order is not guaranteed in the model. To circumvent this, we utilize the lexicographic order of the argument identifier names when instantiating the connector. This order establishes the order of the arguments.
1. Parametric typing implementation is rudimentary, to declare a connector that takes a set of ports instead of a single port, simply add the annotation to the component `@parametric(argn, argm, ...)` where `n`, `m` are the indices (starting at 1) of the arguments that take sets of ports.
1. Invoking motif rules from within DR-BIP by name is possible but only at the level of inter-motif rules, rules cannot be called from within the motif.
