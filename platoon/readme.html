<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>README</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="../pandoc/github.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h2 id="platoon-system">Platoon System</h2>
<p>In this example, we have a single lane road (highway) with multiple cars. Cars organize in platoons. For each platoon there is a leader car which governs the speed of the entire platoon.</p>
<p>All cars initially form a single platoon. A single car can non-deterministically choose to split from the platoon, forming its own platoon.</p>
<p>Platoons which are close merge together to form a single platoon.</p>
<p>Examples are described in details in the <a href="https://www-verimag.imag.fr/~sifakis/papers_pdfs/isola18.pdf">ISOLA’18 paper:</a></p>
<blockquote>
<p>Rim El Ballouli, Saddek Bensalem, Marius Bozga, Joseph Sifakis: <em>Four Exercises in Programming Dynamic Reconfigurable Systems: Methodology and Solution in DR-BIP</em>. ISoLA (3) 2018: 304-320</p>
</blockquote>
<h3 id="instantiating-the-platoon-system-model">Instantiating the Platoon System model</h3>
<p>The DR-BIP model is composed of:</p>
<ol type="1">
<li>A <code>.bip</code> model is provided to provide a blueprint of the static BIP model, including atomic components and connectors.</li>
<li>A <code>.drbip</code> model that includes the relevant motifs, interaction, reconfiguration and global inter-motif rules.</li>
<li>A <code>.cpp</code> file which include conditions, structures (maps) and addressing function implementations for usage with motifs.</li>
</ol>
<p>The Platoon System model can be found in <a href="model/core">model/core</a> and its’ parameters are instantiated to fit our execution.</p>
<p>To instantiate a test model perform:</p>
<pre><code>make gentestmodel</code></pre>
<p>The parameters of the Platoon System are as follows:</p>
<ul>
<li><code>dyn</code> : 0 to disable splits (disables reconfiguration), 1 to enable splits;</li>
<li><code>cars</code> : fixed number of cars on the road;</li>
<li><code>dist</code> : initial distance between cars (in float, i.e.: <em>0.5f</em>), the merge distance is <em>0.5f</em>;</li>
<li><code>movemin</code>-<code>movemax</code>: an interval specifying the possible number of steps before a car can merge (regulating reconfiguration frequency), each follower is assigned a number between these two; and</li>
<li><code>mergedist</code> : the distance at which platoons are merged.</li>
</ul>
<p>To instantiate a model with the specific parameters you must provide a value for <strong>ALL</strong> parameters:</p>
<pre><code>make genmodel dyn=1 cars=100 dist=1.0f movemin=150 movemax=250 mergedist=0.5f</code></pre>
<blockquote>
<p><strong>Note:</strong> the <code>cleanmodel</code> target can be used to remove all model created files as they are normally not removed when performing a clean.</p>
</blockquote>
<h3 id="executing-the-platoon-system">Executing the Platoon System</h3>
<p>With the model instantiated, we now proceed to compile and execute it.</p>
<h4 id="code-generation">Code generation</h4>
<p>To generate the C++ files by compiling both BIP and DR-BIP files execute:</p>
<pre><code>make gencode</code></pre>
<p>The code generation works using 2 sub-phases.</p>
<ol type="1">
<li>First, the BIP compiler is used to generate the BIP engine and types from the <code>.bip</code> file (target: <code>genbip</code>).</li>
<li>Second, the DR-BIP compiler is used to generate the DR-BIP engine and copy the relevant files (target: <code>gendrbip</code>).</li>
</ol>
<h4 id="code-compilation-and-execution">Code Compilation and Execution</h4>
<p>Executing the system will run the system until it deadlocks (note that the Platoon System never deadlocks, so it runs indefinitely). To execute it use:</p>
<pre><code>make execute</code></pre>
<p>To limit the execution to a specific amount of BIP interactions use:</p>
<p>It may be preferred to limit the BIP system execution if so simply using the <code>limit</code> target with the <code>N</code> parameter to specify the cutoff. For example, to limit the execution to 15, use:</p>
<pre><code>make limit N=15</code></pre>
<h4 id="tracing">Tracing</h4>
<p>If you wish to generate execution traces (of length <code>N</code> BIP interactions) the processing is automatically done using:</p>
<pre><code>make trace N=1000</code></pre>
<p>This outputs a new <code>.json</code> file with structured information about the trace.</p>
<p>To view all traces (if you have Python3) it is possible to use the <a href="../tools/trace-viewer">web tool</a> automatically using:</p>
<pre><code>make viewtraces</code></pre>
<p>Then opening a browser on: <a href="http://0.0.0.0:8000">http://0.0.0.0:8000</a>.</p>
<p>The score graph is shown first, by clicking on it, it closes allowing you to explore the other graphs:</p>
<ul>
<li>Press <code>s</code> to view the action trace – sequence of actions applied. Clicking on a red bar on top displays the global view, while clicking on a green bar displays the reconfiguration view of the system (after the reconfiguration). You can use arrows to navigate snapshots. Clicking on interaction rules displays the created connector.</li>
<li>Press <code>h</code> to view a heat map view of the action trace.</li>
<li>Press <code>p</code> to view the score plot again.</li>
</ul>
<p>A set of example traces is provided with the <a href="../tools/trace-viewer/">web tool</a>. To view them go to the folder:</p>
<pre><code>cd ../tools/trace-viewer</code></pre>
<p>And run <code>make</code>.</p>
<p><strong>Replicating the Scenarios from the Paper</strong></p>
<p>To replicate the traces provided in the paper for the two scenarios, proceed as follows.</p>
<p><em>Scenario 1</em>: Cars are initially separated by less than the merge distance.</p>
<pre><code>make genmodel dyn=1 cars=1000 dist=0.3f movemin=150 movemax=200 mergedist=0.5f
make gencode
make trace N=25000 TPREFIX=S1-</code></pre>
<p><em>Scenario 2</em>: Cars are initially separated by twice the merge distance.</p>
<pre><code>make genmodel dyn=1 cars=1000 dist=1.0f movemin=150 movemax=200 mergedist=0.5f
make gencode
make trace N=3000 TPREFIX=S2-</code></pre>
<p>You can now view both traces with:</p>
<pre><code>make viewtraces</code></pre>
<h4 id="performance-analysis">Performance Analysis</h4>
<p>The system can be run in performance mode, hiding all print statements and logging information in counters that are printed at the end of the execution stopping after <code>N</code> BIP executions.</p>
<pre><code>make bench N=1000</code></pre>
<p>A full benchmark script (<a href="bench.sh">bench.sh</a>) is provided to execute benchmarking for various number of parameters and outputs CSV data.</p>
<p>To execute the benchmark script (which may take <strong>30-60</strong> minutes) you need to provide a number of executions per combinations of parameters, and you can optionally use <code>tee</code> to store the output in a file:</p>
<pre><code>./bench.sh 10 | tee bench.csv</code></pre>
<p>The generated CSV file can be analyzed and plotted in <a href="../plot">../plot</a>.</p>
<h3 id="general-implementation-details-when-writing-dr-bip-models">General Implementation Details when Writing DR-BIP models</h3>
<blockquote>
<p><strong>Note:</strong> This section is intended for those wanting to write their own models or rules.</p>
</blockquote>
<p>The used version of the BIP compiler is built around the new-glue branch and as such has certain restrictions when inferring types from the expressions in the BIP model. Further restrictions are due to design decisions for DR-BIP. They are as follows:</p>
<ol type="1">
<li>Connectors are parametrized by <em>atomic components</em> and not <em>ports</em>. The generated code will utilize <strong>only</strong> the first declared instance in the compound component to infer ports. If the same components use different ports then it is required to generate different types.</li>
<li>Due to the new-glue being based on expressions, order is not guaranteed in the model. To circumvent this, we utilize the lexicographic order of the argument identifier names when instantiating the connector. This order establishes the order of the arguments.</li>
<li>Parametric typing implementation is rudimentary, to declare a connector that takes a set of ports instead of a single port, simply add the annotation to the component <code>@parametric(argn, argm, ...)</code> where <code>n</code>, <code>m</code> are the indices (starting at 1) of the arguments that take sets of ports.</li>
<li>Invoking motif rules from within DR-BIP by name is possible but only at the level of inter-motif rules, rules cannot be called from within the motif.</li>
</ol>
</body>
</html>
